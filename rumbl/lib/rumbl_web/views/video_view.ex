defmodule RumblWeb.VideoView do
  use RumblWeb, :view #We know that it will render each like index, edit,delete new as functions

  def category_select_options(categories) do
    for category <- categories, do: {category.name, category.id}
  end
end
