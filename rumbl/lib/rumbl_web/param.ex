defimpl Phoenix.Param, for: Rumbl.Multimedia.Video do #WE have implemnted a spefic version of the protol that works for the video. As long as we have a to_param function we are good. This means that we can keep our changes in videos kept to one spot instead!
    def to_param(%{slug: slug, id: id}) do
        "#{id}-#{slug}"
    end
end