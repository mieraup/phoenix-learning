defmodule RumblWeb.Auth do
    import Plug.Conn
    import Phoenix.Controller;
    alias RumblWeb.Router.Helpers, as: Routes #Can not import RumblWeb.Router.Helpers because that would cause a circular dependency error. This is like forward declaration in C.
    def init(opts), do: opts  # Not sure what this done. REmeber though that
    #Init is the setting up of the plug, setup once AT THE BEGINNING OF THE PLUG SO THAT EXPENSIVE ACTIONS ARE DONE ONLY ONCE
    #Init is for setting up COMPILE time options

    def call(conn, _opts) do
        user_id = get_session(conn, :user_id) #Checks if stored in session

        cond do
            user = conn.assigns[:current_user] -> put_current_user(conn, user)
            
            user = user_id && Rumbl.Accounts.get_user(user_id) -> put_current_user(conn, user) # I don't like this at all, what we are doing is saying that if that user exists in the database and wer'e passing that user, auto login. This is designed to help the testing but I'm not sure I really like this at all


            #We are honouring that if that user is in the conn.assigns we treat it as a logged in user.

            true -> assign(conn, :current_user, nil)
            end
    end

    def login(conn, user) do
        conn
        |> put_current_user(user) #not sure what : before or after a name means yet. It is an atom.
        |> put_session(:user_id, user.id)
        |> configure_session(renew: true)
    end

    defp put_current_user(conn, user) do
        token = Phoenix.Token.sign(conn, "user socket", user.id)
        
        conn
        |> assign(:current_user, user)
        |> assign(:user_token, token) #Holds signed in User ID
    end

    def logout(conn) do
        configure_session(conn, drop: true)
    end
    
    def authenticate_user(conn, _opts) do
        if conn.assigns.current_user do
            conn # return connection
        else 
            conn
            |> put_flash(:error, 'You must be logged in to access that page')
            |> redirect(to: Routes.page_path(conn,:index)) #Redirects to home page since :index atom is used
            |> halt() #Prevent downstream actions from being taken. This action is unauthorized
        end
    end
end