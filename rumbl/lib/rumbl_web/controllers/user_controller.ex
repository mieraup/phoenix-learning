defmodule  RumblWeb.UserController do
    use RumblWeb, :controller # We're defining our module and that we're using the Controller api
    
    alias Rumbl.Accounts
    alias Rumbl.Accounts.User

    plug :authenticate_user when action in [:index, :show] # we now plug in authebntication before every action and can adjust what we can plug in without worrying about updating it in all the spots
    #There is only one spot we have to worry about updating. That is right
    #here.

    def index(conn, _params) do
        users = Accounts.list_users()
        render(conn,"index.html",users: users)
    end

    def show(conn, %{"id" => id}) do
        user = Accounts.get_user(id)
        render(conn, "show.html", user: user)
    end

    def new(conn, _params) do
        #Changesets allow database to manage everything about it rgarding to database including  validations, paameter checkting 
        changeset = Accounts.change_registration(%User{},%{})
        render(conn, "new.html", changeset: changeset)
    end

    def create(conn, %{"user" => user_params}) do
        case Accounts.register_user(user_params) do
            {:ok, user} -> #If we can match the atom and user then we proceed otherwise we do?
            #We rerender new and pass in changeset. That page is responsible for error messages
            # This is really nice how clean it is.
                conn
                |> RumblWeb.Auth.login(user) #This is amazing, added login on a single line
                |> put_flash(:info, "#{user.name} created!")
                |> redirect(to: Routes.user_path(conn, :index))
            
            {:error, %Ecto.Changeset{} = changeset} ->
                render(conn, "new.html", changeset: changeset)
        end
    end


end