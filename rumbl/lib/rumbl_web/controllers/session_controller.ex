defmodule RumblWeb.SessionController do
    use RumblWeb, :controller
    
    def new(conn, _) do
        render(conn,"new.html") #renders login form
    end

    def create(conn,
            %{"session" => %{"username" => username, "password" => pass}}) do
        case Rumbl.Accounts.authenticate_by_username_and_pass(username, pass) do
            {:ok, user} -> #Here we are not worying about biz logic, we translate biz response into actions via pattern matching
                conn
                |> RumblWeb.Auth.login(user)
                |> put_flash(:info, "Welcome back!")
                |> redirect(to: Routes.page_path(conn, :index))
            {:error, _reason} -> 
                conn
                |> put_flash(:error, "Invalid username/password combination")
                |> render("new.html")
        end            
    end

    def delete(conn, _) do
        conn
        |> RumblWeb.Auth.logout()
        |> redirect(to: Routes.page_path(conn, :index)) #Redirecting to page resource index. How beautiful is that?
    end
end