defmodule Rumbl.Multimedia.Category do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query 

  schema "categories" do
    field :name, :string

    timestamps()
  end

  def alphabetical(query) do
    # this is a query , I am not actually using it yet. This is like code that hasn't been executed yet.
    from c in query, order_by: c.name #Without the import Ecto.Query have Compilation error. c must be a variable exposed thru Ecto.Query
  end

  @doc false
  def changeset(category, attrs) do
    category
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end
