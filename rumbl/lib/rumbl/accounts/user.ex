defmodule Rumbl.Accounts.User do
    use Ecto.Schema
    import Ecto.Changeset


    schema "users" do # This creates a struct that MUST have :name and :username  which are the keys username and name.
        field :name, :string
        field :username, :string
        field :password, :string, virtual: true #This makes it exist in schema but NOT database (since we do not want to write the users password in insecure way.)
        field :password_hash, :string
        timestamps() # has createdAt and updatedAt as additional fields (automatically)
        #ecto by default also create a primary key calle "id" this is what allows identifying the data uniquely?

        #databases should be treated with as much respect as code.

        #use the comment ecto.gen.migration create_users
    end
# Structs prevent misspelling of key/value whereas maps (which are what structs are built ontop) do not provide this promise. Furthermore the promise that structs makes are enforced at COMPILE TIME. So we see where error happens, not down the road when we begin to use it.
    def changeset(user, attrs) do # Allow us to verify the structures integrity before adding to database
        user #takes user structs and whatever attributes are and pipes them thru several functions, 

        #This is the Exto.Changeset.cast function
        #Responsible for non sensitve information processing like username and name.
        
        ### REMBER
        # One changeset per case
        ### 

        |> cast(attrs, [:name, :username]) #only name and user name asre allowd to be cast
        #This means that :name and :username are cast to string (That is their schema type) and then we pipe it thru two functions
        |> validate_required([:name, :username])
        |> validate_length(:username, min: 1, max: 20)
        |> unique_constraint(:username) # Instead of throwing an error will store here instead AND render beautifully on the page.
    end

    def registration_changeset(user, params) do #Migrate users to have password. Keeping this function is nessary after a database upgrade
        user
        |> changeset(params)
        |> cast(params, [:password])
        |> validate_required([:password])
        |> validate_length(:password, min: 6, max: 100)
        |> put_pass_hash()
    end

    defp put_pass_hash(changeset) do
        case changeset do
            %Ecto.Changeset{valid?: true, changes: %{password: pass}} ->
                put_change(changeset, :password_hash, Pbkdf2.hash_pwd_salt(pass))  
                _ -> #We are ignoring the first argument and piping it into changeset. Does that mean we are piping nothing into changeset so that it can continue to be processed unmodified?
                # I do not know.
                changeset
        end
    end

    # Changeset is pending changes Observer code
    # iex(4)> changeset = User.registration_changeset(%User{}, %{
    #...(4)> username: "max", name: "Max", password: "123"      
    #...(4)> })                                                 
    ##Ecto.Changeset<
    #   action: nil,
    #   changes: %{name: "Max", password: "123", username: "max"},
    #   errors: [
    #   password: {"should be at least %{count} character(s)",
    #   [count: 6, validation: :length, kind: :min, type: :string]}
    #  ],
    #  data: #Rumbl.Accounts.User<>,
    # valid?: false
    # >
    # iex(5)> changeset.valid?
    # false
    # iex(6)> changeset.valid 
    # ** (KeyError) key :valid not found in: #Ecto.Changeset<action: nil, changes: %{name: "Max", password: "123", username: "max"}, errors: [password: {"should be at least %{count} character(s)", [count: 6, validation: :length, kind: :min, type: :string]}], data: #Rumbl.Accounts.User<>, valid?: false>. Did you mean one of:




    ## However when valid pasword, automatically generates the password hash. How cool is that. Did it also insert into the database? No. So how do we insert it into the database?
    _ignoredCode = '''
        iex(7)> changeset = User.registration_changeset(%User{}, %{
...(7)>  username: "max", name: "Max", password: "asecret"
...(7)> })
#Ecto.Changeset<
  action: nil,
  changes: %{
    name: "Max",
    password: "asecret",
    password_hash: "$pbkdf2-sha512$160000$qEbYi6rsPbgXyNQonC7YWg$fKd7jiCTnCC0i/HRUj9mN/q.8Zp3GTT2XVRqpwFnJ27Prcs8UQT7VciKWa4vgBX..Wy8LcM4K5NFuT/7mlghHA",
    username: "max"
  },
  errors: [],
  data: #Rumbl.Accounts.User<>,
  valid?: true
>
     '''

# WARNING WARNING
#Applying the new changeset which includes hashed password means that all accounts created previously will be invalid.

# See database below

_ignoredCode1 = '''
     id |                        name                        | username  | password_hash |     inserted_at     |     updated_at      
----+----------------------------------------------------+-----------+---------------+---------------------+---------------------
  1 | Jose                                               | josealim  |               | 2020-04-25 12:44:08 | 2020-04-25 12:44:08
  2 | Bruce                                              | redrapids |               | 2020-04-25 12:45:16 | 2020-04-25 12:45:16
  3 | Chris                                              | mccord    |               | 2020-04-25 12:45:37 | 2020-04-25 12:45:37
  4 | Peter                                              | wolf      |               | 2020-04-25 15:55:58 | 2020-04-25 15:55:58
  5 | wolf                                               | test      |               | 2020-04-25 15:56:17 | 2020-04-25 15:56:17
  6 | aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa | aa        |               | 2020-04-25 16:00:37 | 2020-04-25 16:00:37
'''

# What we can do instead is go thru all all uses, and add password. T

end