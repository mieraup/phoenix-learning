defmodule RumblWeb.PageControllerTest do
  use RumblWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    #html_response checks that response was 200 content-type is text/html and that contents contains "Welcome to Rumbl.io"
    assert html_response(conn, 200) =~ "Welcome to Rumbl.io!"
  end
end
