Server Rendered Interactive HTML.
    Result extremely light, stable and long code.

Functions

def inc(x), do: x + 1
2 |> inc |> inc |> dec 
|> is pipe trhu operator

Easier to read code when you understand

ingredients
|> mix()
|> bake()

Layers of Phoenix

connection
|> endpoint()
|> router()
|> pipelines()
|> controller()
    |>common_services()
    |> action()

connection
|> find_user()
|> view()
|> template()

Defintion:
    business logic in moudles called contexts

Pattern Matching in elixir

{first,second,third} = {:lions, :tigers, :bears }
Match first with :lions, second with :tigers...

{first, second, :wolf} =  {:lions, :tigers, :bears }
Will fail because it cannot match the :wolf to the :bear? Why?

Pattern Matching:
    Both sides MUST match
    a = "a" true
    b = "a" error
    "a" = a true

    colors = ["red", "blue", "green"]

    [red, blue, green] = colors

    [head|tail] = colors
    head will be "red"
    tail will be blue and green

    data = {:ok, "success"}

    Pattern matchng allows us to grap ONLY what we need and gorget the rest.

Big takeaway
    Web  applications a series of functions.

Folder breakdown Phoenix View
    assets
        (Browser files like JS and CSS)
    config
        (Phoenix configuration goes into later)
    lib
        name
            (Long running logic, biz code goes here)
        name_web
            (Controllers, views and templates go here)
    test
        (Tests go here)

Folder breakdown Elixir View
    *** Phoenix projects are Elixir projects. Just like express apps are nodejs apps.

    lib
        name
        name_web
            endpoint.ex
            ...
        name.ex
        name_web.ex
    mix.exs
    mix.lock
    test (test folder same as above)

.ex compiled to Elixer code. Transformed to .beam files that Erlang VM runs.
.exs basic infom about the project, compiled in memory. Designed for quick changing scripts.


Enviromental Configuration (config)
prod(uction), dev(elop) and test(ing) enviroments.
Switch by setting MIX_ENV enviromental varaible.

config.exs contains application wide configuration information and ENDPOINTS

enpoints boundary where web server hands off connection to our applcation.


Here is how it goes

lib/hello_web/endpoint.ex
              router.ex
            controllers/hello_controller.ex
            hello_view.ex
            templates/hello/world.html/eex


When adding new route need to provide in router.ex
Add controller, add view and and template file for view.

----------------------


view is a module that converts data into a format that end user will consume

Template is a function on that module, that can be created from a raw markup, or some other templating engine.

<%=%> Execute code within %= and % and injects results into the tag 
<% %> Execute code but DO NOT INJECT RESULT. Used for side effect.

