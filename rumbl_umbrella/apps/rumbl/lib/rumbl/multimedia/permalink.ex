defmodule Rumbl.Multimedia.Permalink do # We defined custom type
    @behavior Ecto.Type

    def type, do: :id #Building on top of ID

    def cast(binary) when is_binary(binary) do #When extermanl data based in, invoked ALSO when queiresare called or by the cast function in changests
        case Integer.parse(binary) do
            {int, _} when int > 0 -> {:ok, int} #Grabs the integer from the prefix and ignores rest of the string.
            _ -> :error
        end
    end

    def cast(integer) when is_integer(integer) do
        {:ok, integer}
    end

    def cast(_) do
        :error
    end

    def dump(integer) when is_integer(integer) do #When data sent to database this is called
        {:ok, integer}
    end

    def load(integer) when is_integer(integer) do #When data is loaded fromdatabase
        {:ok, integer}
    end
end