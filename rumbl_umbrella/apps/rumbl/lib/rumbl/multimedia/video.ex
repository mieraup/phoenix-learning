defmodule Rumbl.Multimedia.Video do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, Rumbl.Multimedia.Permalink, authgenerate: true} #WE are defining primary key, protocol for casting and indicating that database autogenerates it for us.
  schema "videos" do
    field :description, :string
    field :title, :string
    field :url, :string
    field :slug, :string

    belongs_to :category, Rumbl.Multimedia.Category 
    belongs_to :user, Rumbl.Accounts.User #Creates association between User and video
    # this is called a foreign key.

    #Can roll forward and back. This allows for reverting updates.

    #WE WANT TO MAKE SURE DEPENDEICES ARE ONE WAY

    has_many :annotations, Rumbl.Multimedia.Annotation #We are saying that we have many annotations for each video
  

    timestamps()
  end

  @doc false
  def changeset(video, attrs) do
    video
    |> cast(attrs, [:url, :title, :description, :category_id]) #Whitelist what may be allowed in
    #user_id is not castable or required because comes from internal data(the application). The application is responsible for associating the right user to each video.
    |> validate_required([:url, :title, :description])
    |> assoc_constraint(:category) #Makes sure that category follows it's constaint of only having a name that matches category.
    #If user updates or middle of udating and categories change, want to deliver nice message to user indicating why the error occured. 
    #Failing to throw an error leads to database INCONSISTENCIES. that is bad. Let us not do that
    #Swallowing the error leads to  bad UX. Let us NOT do that either.
    |> slugify_title()
  end

  defp slugify_title(changeset) do
    case fetch_change(changeset, :title) do
        {:ok, new_title} -> put_change(changeset, :slug, slugify(new_title))
        :error -> changeset
    end
  end

  defp slugify(str) do #changesets allow us specify how to change for each type of data in each possible senerio!

  #Changesets VALIDATE DATA BUT NOT DATA INTEGRITY. THAT is left to the database as constraints
    str
    |> String.downcase()
    |> String.replace(~r/[^\w-]+/u, "-")
  end
end
