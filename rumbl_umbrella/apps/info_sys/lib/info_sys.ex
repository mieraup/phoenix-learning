defmodule InfoSys do

  @backends [InfoSys.Wolfram] #Adding backends easy
  # this is alist of backends. the @ is a module attribute
  alias InfoSys.Cache

  defmodule Result do
    defstruct score: 0, text: nil, backend: nil #Define a result struct that has three parts. score (relavent), text (the actual text) and backend (which holds the rsults)
  end

  def compute(query, opts \\ []) do # Main entry point to this module
    #Goal is to wait for all backends that complete their quey within a certain period of time. Ignoring tardy or offline backends
    timeout = opts[:timeout] || 10_000

    opts = Keyword.put_new(opts, :limit, 10) # we are making sure the limit atom exists

    backends = opts[:backends] || @backends # Not sure what we are doing here, getting the backend option or the locally defined backends.

    {uncached_backends, cached_results} = fetch_cached_results(backends, query, opts)

    uncached_backends
    |> Enum.map(&async_query(&1, query, opts)) #What we are doing is piping the list of backends thru the query and passing in the query and opts
    # the & is another way to write anonoumos function
    |> Task.yield_many(timeout) #automatically wait for results until timeout or all backends completed
    #This task must return a tuple list that contains task and result.
    |> Enum.map(fn {task, res} -> res || Task.shutdown(task, :brutal_kill) end) #Either we have gotten a result and the task shut itself down or we kill it
    |> Enum.flat_map(fn {:ok, results} -> results 
    _ -> [] 
    end) #Flatten so we just have a list of results
    |> write_results_to_cache(query, opts)
    |> Kernel.++(cached_results) #What does this line do
    |> Enum.sort(&(&1.score >= &2.score)) #sort the results in ascending order
    |> Enum.take(opts[:limit]) #take the first limit results and return
  
  end

  defp async_query(backend, query, opts) do #spawns off the proccesses and do the work
  # We are constructing a supervisor that is asynchronous that if the task dies, will not be restarted. If it fails to die, will be killed, we pass the list of children which are labled backend, the the query and options 
    Task.Supervisor.async_nolink(InfoSys.TaskSupervisor, backend, :compute,  [query, opts], shutdown: :brutal_kill) # Not sure what brutal_kill does but seems to forcemfully kill
  end

  defp fetch_cached_results(backends, query ,opts) do
    {uncached_backends, results} = 
      Enum.reduce(
        backends,
        {[], []},
        fn backend, {uncached_backends, acc_results} ->
          case Cache.fetch({backend.name(), query, opts[:limit]}) do
            {:ok, results} -> {uncached_backends, [results | acc_results]} #we add it to the actual results and remove backend from uncached
            :error -> {[backend | uncached_backends], acc_results}
          end
      end)
      {uncached_backends, List.flatten(results)} #We are returning cached results and list of backends that the queries need to be refreshed in
  end

  defp write_results_to_cache(results, query, opts) do
    Enum.map(results, fn %Result{backend: backend} = result ->
      :ok = Cache.put({backend.name(), query, opts[:limit]}, result) #We are putting in the exact name, query and limit that we had. That means increasing or decreasing the limit will change the answer

      result
    end)
  end
end
