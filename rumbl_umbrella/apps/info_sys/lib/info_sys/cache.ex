defmodule InfoSys.Cache do
    @clear_interval :timer.seconds(60)

    use GenServer #Think of inheriting except not really, we are importing all of GenServer's functions to be used without full prefix

    def put(name \\ __MODULE__, key, value) do #All caches need read and write thiat is what put and fetch do.
        true = :ets.insert(tab_name(name), {key, value})
        :ok
    end

    def fetch(name \\ __MODULE__, key) do
        {:ok, :ets.lookup_element(tab_name(name), key, 2)} # we are looking for the key which is 1 based. Since first value is key and second is value we want value
        rescue
            ArgumentError -> :error
    end

    def start_link(opts) do #We ensure a :name is present yet we do not know were it comes from
        opts = Keyword.put_new(opts, :name, __MODULE__) # Not sure where :name comes from
        GenServer.start_link(__MODULE__, opts, name: opts[:name]) #WE  set server name to module name for now. This allows a single cache system
    end

    def init(opts) do #For our cache sweeping technology, we are going to have three pieces of information for the state.

    #: intervalu time between sweeps, :timer will hold pid of theprocess and table will hold ets table.
        state = %{
            interval: opts[:clear_interval] || @clear_interval,
            timer: nil,
            table: new_table(opts[:name])
        }
        {:ok, schedule_clear(state)} #We return a map that will hold new state?

        #Caches in etc belong to a single owner and upon the owners death the cache dies also
    end

    def handle_info(:clear, state) do #Purges all info from table and schedules next clearing
        :ets.delete_all_objects(state.table)
        {:noreply, schedule_clear(state)}
    end

    defp schedule_clear(state) do
        %{state | timer: Process.send_after(self(), :clear, state.interval)} # Why is it within the plug and the struct we access the interval with a 
    end

    defp new_table(name) do
        name
        |> tab_name()
        |> :ets.new([
            :set, #REquestio a key/value type of table
            :named_table,
            :public, #pyblically accessible by other threads
            read_concurrency: true, #allow full concurrency
            write_concurrency: true
        ])
    end

    defp tab_name(name), do: :"#{name}_cache"
end