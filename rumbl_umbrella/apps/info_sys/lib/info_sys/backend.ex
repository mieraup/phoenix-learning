defmodule InfoSys.Backend do # WE are defining a typespec. Think of it as an interface, anything that wants to implement a backend MUST implement this.
    @callback name() :: String.t()
    @callback compute(query :: String.t(), opts :: Keyword.t()) ::
        [%InfoSys.Result{}]
end