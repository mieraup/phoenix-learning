defmodule InfoSys.Counter do #Counter server as well as function for interaction with client
    use GenServer, restart: :permanent # Behind the scenes generates a child_spec function 
#The client serves as the API oly to send messages to the process.
    def inc(pid), do: GenServer.cast(pid, :inc) 
    
    # Takes the process id for the server process
    #Takes a single atom command

    # WE send a message to the server and continue prcessing next command
    # Therefor this is ASYNC

    def dec(pid), do: GenServer.cast(pid, :dec)

    def val(pid) do
        GenServer.call(pid,:val) #Synchronous reponse
   end

    def start_link(initial_val) do # Starts the server. Basic A
        GenServer.start_link(__MODULE__, initial_val) # invokes init
    end

    def init(initial_val) do
        Process.send_after(self(), :tick, 1000)
        {:ok, initial_val}
    end

    def handle_info(:tick, val) when val <= 0, do: raise "boom!"

    def handle_info(:tick, val) do
        IO.puts("tick #{val}")
        Process.send_after(self(), :tick, 1000)
        {:noreply, val - 1}
    end

    def handle_cast(:inc, val) do
        {:noreply, val + 1}
    end

    def handle_cast(:dec, val) do
        {:noreply, val - 1}
    end

    def handle_call(:val, _from, val) do
        {:reply, val, val}
    end
end

# OTP needs a start_link function

