defmodule InfoSys.Wolfram do
    import SweetXml

    alias InfoSys.Result

    @behaviour InfoSys.Backend # this looks like we are promising that this system will behave like backend!

    @base "http://api.wolframalpha.com/v2/query"

    @impl true #Not sure what this mean, I think that the behavior is fully implemented?
    def name, do: "wolfram"

    @impl true
    def compute(query_str, _opts) do
        query_str
        |> fetch_xml()
        |> xpath(~x"/queryresult/pod[contains(@title, 'Result') or contains(@title, 'Definitions')]/subpod/plaintext/text()") # Looks like we are going down two paths and then extracting result
        |> build_results()
    end

    defp build_results(nil), do: []

    defp build_results(answer) do
        [%Result{backend: __MODULE__, score: 95, text: to_string(answer)}]
    end

    @http Application.get_env(:info_sys, :wolfram)[:http_client] || :httpc
    defp fetch_xml(query) do
        {:ok, {_, _, body}} = @http.request(String.to_charlist(url(query)))

        body
    end

    defp url(input) do
        "#{@base}?" <> URI.encode_query(appid: id(), input: input, format: "plaintext")
    end

    defp id, do: Application.fetch_env!(:info_sys, :wolfram)[:app_id] #Alright this makes sense, we are setting the env of info sys, sub enviroment wolfram to something 
end