defmodule RumblWeb.Presence do
  @moduledoc """
  Provides presence tracking to channels and processes.

  See the [`Phoenix.Presence`](http://hexdocs.pm/phoenix/Phoenix.Presence.html)
  docs for more details.
  """
  use Phoenix.Presence, otp_app: :rumbl_web,
                        pubsub_server: RumblWeb.PubSub

  def fetch(_topic, entries) do
    users = 
      entries
      |> Map.keys()
      |> Rumbl.Accounts.list_users_with_ids()
      |> Enum.into(%{}, fn user ->
        {to_string(user.id), %{username: user.username}}
      end)
      #We are taking presence entries where is map of user_id and session_metadata pairs

      #pipe thru new function (the list_users_with_ids)
      #Then build a map and add in usernames
      for {key, %{metas: metas}} <- entries, into: %{} do
        {key, %{metas: metas, user: users[key]}}
      end
  end
end
