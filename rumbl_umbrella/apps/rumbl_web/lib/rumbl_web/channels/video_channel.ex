defmodule RumblWeb.VideoChannel do
    use RumblWeb, :channel
    alias Rumbl.{Accounts, Multimedia}
    alias RumblWeb.AnnotationView

    def join("videos:" <> video_id, params, socket) do #matches the video: part of the string and dumps the rest of the string to video_id
        send(self(), :after_join)
        last_seen_id = params["last_seen_id"] || 0
        video_id = String.to_integer(video_id)
        video = Multimedia.get_video!(video_id)

        annotations = 
            video
            |> Multimedia.list_annotations(last_seen_id)
            |> Phoenix.View.render_many(AnnotationView, "annotation.json") # Here we are asking Phoenix to collect all the individual annotations and combine them into a single JSONish thing


        {:ok, %{annotations: annotations}, assign(socket, :video_id, video_id)}
    end

    def handle_in(event, params,socket) do # we get all input here and then if user exists THEN we get input in handle_in
        user = Accounts.get_user!(socket.assigns.user_id)
        handle_in(event,params, user, socket)
    end

    def handle_in("new_annotation", params, user, socket) do #We aren't saving to database, so we just broadcast to whoever is connected.

    #Uses pub/sub system which works at a lower layer of abstraction that allows all the servers to coordiante on sending messages out to the clients that are interested in the topic
        case Multimedia.annotate_video(user, socket.assigns.video_id, params) do
            {:ok, annotation} ->
                broadcast_annotation(socket, user, annotation)
                Task.start(fn -> compute_addition_info(annotation, socket) end)
                {:reply, :ok, socket}
            {:error, changeset} ->
                {:reply, {:error, %{errors: changeset}}, socket}
        end
    end

    defp broadcast_annotation(socket, user, annotation) do
        broadcast!(socket, "new_annotation", %{
            id: annotation.id,
            user: RumblWeb.UserView.render("user.json", %{user: user}),
            body: annotation.body,
            at: annotation.at
        })
    end

    defp compute_addition_info(annotation, socket) do
        for result <-
            InfoSys.compute(annotation.body, limit: 1, timeout: 10_000) do

            backend_user = Accounts.get_user_by(username: result.backend.name() )        #We are getting the user to post the annotation
            attrs = %{body: result.text, at: annotation.at}

            case Multimedia.annotate_video( #Then we broadcast the annotation
                backend_user, annotation.video_id, attrs) do
                
                {:ok, info_ann} ->
                    broadcast_annotation(socket, backend_user, info_ann)
                {:error, _changeset} -> :ignore
            end
            
        end
    end

    def handle_info(:after_join, socket) do  # We are tracking presense and we are assigning bare bones user connection information since it will be maintained for the life of the user.

    #The presense function tracks pings and such and that is point, we no longer worry about presense functions.

    #Perhaps the reason we access the presense AFTER user is successfully joined is to not have presense constantly fluxtuation
        push(socket, "presence_state", RumblWeb.Presence.list(socket))
        {:ok, _ } = RumblWeb.Presence.track(
            socket,
            socket.assigns.user_id,
            %{device: "browser"})
        {:noreply, socket}
    end
end