defmodule RumblWeb.WatchView do
    use RumblWeb, :view

    def player_id(video) do
        ~r{^.*(?:youtu\.be/|\w+/|v=)(?<id>[^#&?]*)} #I'm creating a regular expression inline. this makes so much sense since regular expressions are used so often.
        |> Regex.named_captures(video.url)
        |> get_in(["id"])
    end
end