defmodule RumblWeb.AuthTest do
    use RumblWeb.ConnCase, async: true
    alias RumblWeb.Auth

    setup %{conn: conn} do 
        conn = 
            conn 
            |> bypass_through(RumblWeb.Router, :browser)
            |> get("/")
        {:ok, %{conn: conn}}
    end

    test "authenticate_user halts when no current_user exists", %{conn: conn} do
        conn = Auth.authenticate_user(conn,[]) #Without setup you'll need to endup reimplemting a mirror pipeline so that nessary transformations that authentication relies on occurs.
        #Trying to have a mirror pipeline and rembering to update both pipelines whenever you change one is a really bad idea and violates DRY.
        assert conn.halted
    end

    test "authenticate_user for existing current_user", %{conn: conn} do
        conn =
            conn
            |> assign(:current_user, %Rumbl.Accounts.User{})
            |> Auth.authenticate_user([])
        refute conn.halted
    end

    test "login puts the user in the session", %{conn: conn} do
        login_conn = #We craete a connection by loggin in and putting user in the connection. 
            conn
            |> Auth.login(%Rumbl.Accounts.User{id: 123})
            |> send_resp(:ok, "")
        next_conn = get(login_conn, "/") #we make sure user sticks around by responding in the same connection
        assert get_session(next_conn, :user_id) == 123
    end

    test "logout drops the session", %{conn: conn} do
        logout_conn = 
            conn
            |> put_session(:user_id, 123)
            |> Auth.logout()
            |> send_resp(:ok, "")
        next_conn = get(logout_conn, "/")
        refute get_session(next_conn, :user_id)
    end

    test "call places user from session into assigns", %{conn: conn} do #Makes sure that upon startup if there is a user add then to the connection
        user = user_fixture()
        conn = 
            conn
            |> put_session(:user_id, user.id)
            |> Auth.call(Auth.init([]))

        assert conn.assigns.current_user.id == user.id
    end

    test "call with no session sets current_user assign to nil", %{conn: conn} do #makes sure a logged out user stays logged out
        conn = Auth.call(conn, Auth.init([]))
        assert conn.assigns.current_user == nil
    end
end