defmodule RumblWeb.Channels.VideoChannelTest do
    use RumblWeb.ChannelCase
    import RumblWeb.TestHelpers #Not async BECAUSE channels share a single connection only

    setup do
        user = insert_user(name: "Gary")
        video = insert_video(user, title: "Test")
        token = Phoenix.Token.sign(@endpoint, "user socket", user.id)
        {:ok, socket} = connect(RumblWeb.UserSocket, %{"token" => token})

        {:ok, socket: socket, user: user, video: video}
    end

    test "join replies with video annotations", %{socket: socket, video: vid, user: user} do
        for body <- ~w(one two) do
            Rumbl.Multimedia.annotate_video(user, vid.id, %{body: body, at: 0})
        end
        {:ok, reply, socket} = subscribe_and_join(socket, "videos:#{vid.id}", %{}) #From the video channel

        assert socket.assigns.video_id == vid.id
        assert %{annotations: [%{body: "one"}, %{body: "two"}]} = reply
    end

end