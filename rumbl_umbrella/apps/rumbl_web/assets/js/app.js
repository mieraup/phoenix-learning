// We need to import the CSS so that webpack will load it.
// The MiniCssExtractPlugin is used to separate it out into
// its own CSS file.
import "../css/app.scss"

// webpack automatically bundles all modules in your
// entry points. Those entry points can be configured
// in "webpack.config.js".
//
// Import deps with the dep name or local files with a relative path, for example:
//
//     import {Socket} from "phoenix"
//     import socket from "./socket"
//
import "phoenix_html" //Provides functionality to the browser button responses. //That was something that I completely forgot about, that phoenix_html is doing javascript to do button responses.

import socket from "./socket"
import Video from "./video"

Video.init(socket, document.getElementById("video"))

