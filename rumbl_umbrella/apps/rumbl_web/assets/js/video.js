import Player from "./player"
import {Presence} from "phoenix"
let Video = {
    init(socket, element){
        if(!element) return;
        let playerId = element.getAttribute("data-player-id")
        let videoId = element.getAttribute("data-id")
        socket.connect()
        Player.init(element.id, playerId, () => {
            this.onReady(videoId, socket)
        })
    },

    onReady(videoId, socket){
        let msgContainer = document.getElementById("msg-container")
        let msgInput = document.getElementById("msg-input")
        let postButton = document.getElementById("msg-submit")
        let lastSeenId = 0
        let userList = document.getElementById("user-list")
        let vidChannel = socket.channel("videos:"+videoId, () => {
            return {last_seen_id: lastSeenId} //Keeping the correct scoping in mind using the arrow function. Which has access to the lastSeenId that is above it
        })

        postButton.addEventListener("click", e=> {
            let payload = {body:msgInput.value, at: Player.getCurrentTime()} //Takes userss input sends it to server and clears textbox
            vidChannel.push("new_annotation", payload).receive("error", e => console.log(e)) //Fake synchronise response, really a promiseish like thing. Makes code much more readable.
            msgInput.value = ""
        })

        vidChannel.on("new_annotation", (resp) => { //We can send different  types of messages and have event handlers for these types of messages. This would allow passive serverside discovery of client capabilities. Just like HTML and CSS detection
            //Upon receiving a message we render it
            lastSeenId = resp.id
            this.renderAnnotation(msgContainer, resp)
        })

        let presence = new Presence(vidChannel)

        presence.onSync(() => { //Get list and user a function that gets id and meta data`
            userList.innerHTML = presence.list((id, {user: user, metas: [first,...rest]})  => {
                let count = rest.length + 1;
                return `<li>${user.username}: (${count})`
            }).join("")
        })

        //Right now we fail to address the issue when the server or client abrubtly disconnect
        msgContainer.addEventListener("click", e => {
            e.preventDefault()
            let seconds = e.target.getAttribute("data-seek") || e.target.parentNode.getAttribute("data-seek")
            if(!seconds) return
            Player.seekTo(seconds);
        })
        vidChannel.on("ping", ({count}) => console.log("PING", count))
        vidChannel.join().receive("ok", resp => {
            let ids = resp.annotations.map(ann => ann.id)
            if(ids.length > 0) { lastSeenId = Math.max(...ids)} //... is spread operator
            this.scheduleMessages(msgContainer,resp.annotations)
        }).receive("error", reason => console.log("join failed", reason))
    },

    esc(str){
        let div = document.createElement("div")
        div.appendChild(document.createTextNode(str))
        return div.innerHTML
    },

    renderAnnotation(msgContainer, {user, body, at}){
        //TODO Render
        let template = document.createElement("div")

        template.innerHTML = `
        <a href="#" data-seek="${this.esc(at)}">
            [${this.formatTime(at)}] 
            <b>${this.esc(user.username)}</b>: ${this.esc(body)}
        </a>
        `

        msgContainer.appendChild(template)
        msgContainer.scrollTop = msgContainer.scrollHeight; //Scroll so that next message is in view
        //We are escaping user input to prevent XSS which would be really bad (tm)
        
    },

    scheduleMessages(msgContainer, annotations){
        clearTimeout(this.scheduleTimer) //Clear current timeout for this.scheduleTimer
        this.scheduleTimer = setTimeout(() => { //Start new timer every 1 second
            let ctime = Player.getCurrentTime() 
            let remaining = this.renderAtTime(annotations, ctime, msgContainer) //Render ones at or before current time
            this.scheduleMessages(msgContainer, remaining) //Pass remaining to schedule messages
        }, 1000)
    },

    renderAtTime(annotations, seconds, msgContainer){
        return annotations.filter( ann => {
            if(ann.at > seconds){ //Greater then current seconds keep
                return true
            } else {
                this.renderAnnotation(msgContainer,ann)
                return false //Otherwise discard
            }
        })
    },
    
    formatTime(at){
        let date = new Date(null)
        date.setSeconds(at / 1000)
        return date.toISOString().substr(11,8);

    }
}

export default Video